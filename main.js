const { BrowserWindow, app } = require('electron')
const { isNull } = require('lodash')
require('./app.js')

let mainWindow = null
let hostWindow = null

function main() {
  mainWindow = new BrowserWindow({autoHideMenuBar: true})
  mainWindow.title = 'J Game Board'
  mainWindow.loadURL(`http://localhost:3000/#/board`)
  mainWindow.on('close', event => {
    mainWindow = null
    if (!isNull(hostWindow)) {
        hostWindow = null
    }
  })
  hostWindow = new BrowserWindow({autoHideMenuBar: true})
  hostWindow.title = 'Host Window - DO NOT SCREENSHARE THIS!'
  hostWindow.loadURL('http://localhost:3000/')
  hostWindow.on('close', event => {
    hostWindow = null
    if (!isNull(mainWindow)) {
        mainWindow = null
    }
  })
}

app.on('ready', main)