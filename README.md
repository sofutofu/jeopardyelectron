# Jeopardy! Practice Game

A really lazy Electron version of the excellent [Jeopardy! Practice Game](https://github.com/theGrue/jeopardy) created by [@theGrue](https://github.com/theGrue), so you don't need to know what npm is to be the host. Also includes a few tweaks to improve the experience in remote screenshare play as well.

## Downloading and running

- [Windows](https://gitlab.com/sofutofu/jeopardyelectron/-/blob/e3ccaae4f1d5e09e14db56ce70ca878d5eca20c8/dist/Jeopardy%200.9.2-1.exe)

All prebuilt binaries are available in the `dist` folder. (Yes, the binaries are in the repo, sorry. Part of this being lazy is I don't want to figure out GitLab releases and the piddly 10MB upload limit.)

Mac binaries are possible, but you need to BYOMac as my 2011 MBP has kicked the bucket.

## Latest updates

2022-04-14: v0.9.2-1 hotfix to clear up SSL issues with Electron.

2021-11-22: v0.9.2 binary is available for PC. This fixes the display of media clues (since they're rarely if ever available from J-Archive) and cleans up some visual elements of the Electron windows for a cleaner look.

2021-08-30: v0.9.1 binary is now available for PC. This includes QOL upgrades for hosts to make it easier to identify how many media clues in a selected game ahead of time, as this has occasionally been an issue in our Discord games.

### Tips and tricks

- Make sure you screenshare ONLY the main board window, not the host window! On startup the main board window will display a YouTube clip of the Jeopardy theme for you to play while you get things set up.

- If you have a small single monitor (e.g. 13" non-Retina MBP or MBA) you should make sure to screenshare the board window (not your screen), then you can put it behind the host window and not really look at it for the rest of the game.

- You may need to give the app access to go through the firewall. Private network access is fine. It runs on port 3000 if that's meaningful to you.

## Disclaimer

Borrowing this one from Josh and J! Archive, just in case.

> The Jeopardy! game show and all elements thereof, including but not limited to copyright and trademark thereto, are the property of Jeopardy Productions, Inc. and are protected under law. This website is not affiliated with, sponsored by, or operated by Jeopardy Productions, Inc.
